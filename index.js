function calculateGrade() {
    let mark = parseFloat(document.getElementById('mark').value);
    let grade;

    if (mark >= 90 && mark <= 100) {
        grade = 'A+';
    } else if (mark >= 80 && mark <= 89) {
        grade = 'A';
    } else if (mark >= 70 && mark <= 79) {
        grade = 'B+';
    } else if (mark >= 60 && mark <= 69) {
        grade = 'B';
    } else if (mark >= 50 && mark <= 59) {
        grade = 'C+';
    } else if (mark >= 40 && mark <= 49) {
        grade = 'C';
    } else if (mark >= 30 && mark <= 39) {
        grade = 'D+';
    } else if (mark >= 20 && mark <= 29) {
        grade = 'D';
    } else if (mark < 20) {
        grade = 'E (FAIL)';
    } else {
        grade = 'Invalid Mark';
    }

    document.getElementById('result').innerText = 'Grade: ' + grade;
}

document.addEventListener('keydown', function(event) {
    const key = event.key;
    if (key === 'Enter') {
        calculateGrade();
    }
});
